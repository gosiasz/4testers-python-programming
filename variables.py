first_name = "Małgorzata"
last_name = "Szczęśniak"
age = 45

print(first_name)
print(last_name)
print(age)

# I am defining a set of variables describing my dog
name = "Aleks"
caot_color = "Black/brown"
age_of_years = 6
age_unit = "years"
number_of_paws = 4
weight = 19.9
weight_unit = "kg"
is_male = True
has_chip = True
is_vaccinated = True

print(name, caot_color, age_of_years, age_unit, number_of_paws, weight, weight_unit, is_male, has_chip, is_vaccinated)

# Below I describe my friendship  as a set og variables
friend_name = "Mika"
friend_age_in_years = 3
animal_number = 1
friendship_duration_in_years = 2.5
driving_licence = False
print("Friend's name:", friend_name, sep="\t")
print(friend_name, friend_age_in_years, animal_number, friendship_duration_in_years, driving_licence, sep=';')

# print(friend_name, friend_age_in_years, animal_number, sep='\t')
#
# my_bank_account_total = 6000000
# my_bank_account_total1 = 6_000_000
# my_bank_account_total2 = 6e6
# print(my_bank_account_total, my_bank_account_total1, my_bank_account_total2)
#
# def print_hello():
#     print('Hello')
#
# def say_hello(name):
#     print(f'Hello {name}!')
#
# def upper_word(word):
#     return word.upper()
#
# big_dig = upper_word("dog")

def just_print_word():
    print("4_testers")

